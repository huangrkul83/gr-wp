//Node.js libraries
var del = require('del');
var path = require('path');

// Gulp dependencies
var gulp = require('gulp');
var debug = require('gulp-debug');
var sass = require('gulp-sass');
sass.compiler = require('node-sass');
var sourcemaps = require('gulp-sourcemaps');

//directory mapping. Update as necessary
var devDir = './src/';
var destDir = './www/wp-content/themes/greenrubino/';

gulp.task('sass', function(){
  return gulp.src(devDir + 'assets/scss/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(destDir + 'assets/css/'));
});

gulp.task('scripts', function(){
  return gulp.src(devDir + 'assets/js/*.js').pipe(gulp.dest(destDir + 'assets/js/'));
});

gulp.task('move-files', function(){
  return gulp.src([devDir + '**/**/*.php']).pipe(gulp.dest(destDir));
});

gulp.task('watch', function(){
  gulp.watch(devDir + 'assets/js/*.js', gulp.series('scripts'));
	gulp.watch(devDir + 'assets/scss/**/*.+(scss|sass)', gulp.series('sass'));
	gulp.watch(devDir + '**/**/*.php', gulp.series('move-files'));
});

//Clean Output Tasks
// gulp.task('clean', function() {
//   return del([
//     'dist/**/*',
//     '!dist/media/**'
//   ]);
// });

var serves = gulp.series('sass','scripts','move-files');
// Default task is watch
gulp.task('default', gulp.series(serves,'watch'));
