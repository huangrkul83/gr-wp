<?php
/**
 * views/team/single
 * Single Team View
 * Add super simple Single Team Logic here. Focus on the archive design.
 * This is just extra points.
 *
 * @author    Stephen Scaff
 * @package   jumpoff
 */

namespace Jumpoff;

if ( ! defined( 'ABSPATH' ) ) exit;

get_header();

while (have_posts()) : the_post(); ?>

<main>
  <?php
    $bioImage = get_field("team_image");
    $postTitle = get_the_title();
    $teamPos = get_field("team_position");
    $teamBio = get_field("team_bio");
  ?>
  <div class="team-single">
    <img class="team-single-image" src="<?php echo $bioImage['url']; ?>" /><br>
    <span class="team-single-title title-font"><?php echo $postTitle ?></span><br>
    <span class="team-single-position sub-head-font"><?php echo $teamPos ?></span>
    <div class="team-single-bio sub-head-font"><?php echo $teamBio ?></div>
  </div>
</main>

<?php endwhile; ?>


<?php get_footer(); ?>
