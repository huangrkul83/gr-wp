<?php
/**
 * Post Archive / Index                                                                                                         n
 *
 * @author    Stephen Scaff
 * @package   page
 * @version   2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

get_header(); ?>



<main>

  <h1 class="team-main-title arial">WE ARE THE ALPHA OMEGA</h1>
  <div id="teamGrid" class="team-grid">
    <?php
      $args = array("order" => "ASC", "post_type" => "team", "posts_per_page" => -1);
      $posts_array = get_posts($args);
      foreach($posts_array as $post)
      {
        $bioImage = get_field('team_image', $post);
      ?>

        <div class="cell arial">
          <img class="team-image" src="<?php echo $bioImage['url']; ?>" /><br>
          <span class="team-title"><?php echo $post -> post_title ?></span><br>
          <span class="team-position"><?php echo $post -> team_position ?></span>
        </div>
    <?php
      }
    ?>
  </div>
</main>

<?php get_footer(); ?>
