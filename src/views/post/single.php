<?php
/**
* The default template for single blog posts.
*
* @author    Stephen Scaff
* @package   jumpoff
* @version   1.0.0
*/

if ( ! defined( 'ABSPATH' ) ) exit;

get_header();

while (have_posts()) : the_post(); ?>

<main></main>

<?php endwhile; ?>

<?php get_footer(); ?>
