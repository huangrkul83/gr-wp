menuBtn = document.getElementById("appHeaderMenuBtn");
menuList = document.getElementById("appHeaderNav");
menuBtn.addEventListener("click", menuHandler);

function menuHandler() {
  menuBtn.classList.toggle("change");
  menuList.classList.toggle("app-header-nav-active");
}
