<?php

namespace Jumpoff;

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 *  Post Type: Team
 *
 *  Slug :      Team
 *  Supports : 'title','thumbnail','editor'
 *
 *  @version    1.0
 *  @author     stephen scaff
 */

 add_action( 'init', function() {
  $type = 'team';

  // Call the function and save it to $labels
  $labels = set_post_type_labels('Team', 'Teams');

  $args = [
    'public'             => true,
    'show_in_rest'       => true,
    'rest_base'          => 'teams',
    'rest_controller_class' => 'WP_REST_Posts_Controller',
    'description'        => 'Example Post Type.',
    'hierarchical'       => true,
    'labels'             => $labels,
    'show_ui'            => true,
    'menu_position'      => 3,
    'menu_dashicon'      => 'dashicons-id',
    'menu_icon'          => 'dashicons-id',
    'query_var'          => true,
    'supports'           => array( 'title','thumbnail', 'editor' ),
    'capability_type'    => 'post',
    'can_export'         => true,
    'has_archive'        => true,
    'rewrite'            => array(
      'slug'       => 'team',
      'with_front' => false
    ),
  ];
  register_post_type( $type, $args);
 });
