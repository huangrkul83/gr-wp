<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Formated Dumper (tee hee)
 * @return {string} a parsable string representation of a variable
 */
function jumpoff_dump( $var, $name = '' ) {
	echo '<pre>';
	if ( '' !== $name ) {
		echo $name . ': ';
	}
	var_export( $var );
	echo '</pre>';
}

/**
 * Jumpoff Wp Query
 * View the page's sql query
 */
function jumpoff_wp_query() {
	return $GLOBALS['wp_query']->request;
}
