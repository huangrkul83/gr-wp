<?php
/**
 * views/team/single
 * Single Team View
 * Create a simple get_posts() loop to get your team post type and realted fields
 * refer to the provided design for this view.
 *
 * @author    Stephen Scaff
 * @package   jumpoff
 */

namespace Jumpoff;

if ( ! defined( 'ABSPATH' ) ) exit;

get_header();

?>

<main>

  <h1 class="team-main-title title-font">WE ARE THE ALPHA OMEGA</h1>
  <div id="teamGrid" class="team-grid">
    <?php
      $args = array("order" => "ASC", "post_type" => "team", "posts_per_page" => -1);
      $posts_array = get_posts($args);
      foreach($posts_array as $post)
      {
        $bioImage = get_field('team_image', $post);
      ?>

        <div class="cell">
            <a class="team-link" href="<?php echo get_post_permalink($post); ?>">
              <img class="team-image" src="<?php echo $bioImage['url']; ?>" />
              <div class="team-title title-font"><?php echo $post -> post_title ?></div>
              <div class="team-position sub-head-font"><?php echo $post -> team_position ?></div>
            </a>
        </div>
    <?php
      }
    ?>
  </div>
</main>


<?php get_footer(); ?>
