<?php
/**
 * Views/Shared/Header
 * Main site/app header and nav section.
 *
 * @author    Johnny You
 * @package   Jumpoff
 */

namespace Jumpoff;

if ( ! defined( 'ABSPATH' ) ) exit;

?>

<header class="app-header">
  <div class="app-header-logo"><p class="logo-txt">GR</p></div>
  <div class="app-header-menu-container">
    <div id="appHeaderMenuBtn" class="app-header-menu-btn">
      <div class="app-header-btn-bar1"></div>
      <div class="app-header-btn-bar2"></div>
      <div class="app-header-btn-bar3"></div>
    </div>
  </div>
  <div id="appHeaderNav" class="app-header-nav">
    <ul>
      <li class="app-header-nav-item"><a class="team-link" href="http://willhuanganimator.com/wp-test/team/">Home</a><span class="divider"> |</span></li>
      <li class="app-header-nav-item"><a class="team-link" href="http://willhuanganimator.com/wp-test/team/">About</a><span class="divider"> |</span></li>
      <li class="app-header-nav-item"><a class="team-link" href="http://willhuanganimator.com/wp-test/team/">Work</a><span class="divider"> |</span></li>
      <li class="app-header-nav-item"><a class="team-link" href="http://willhuanganimator.com/wp-test/team/">Contact</a></li>
    </ul>
  </div>
</header>
