<?php
/**
 * Shares/Views/Head
 *
 * Head partial including metas, custom seo fields, wp_head(), etc.
 *
 * @author    Stephen Scaff
 */

namespace Jumpoff;

if ( ! defined( 'ABSPATH' ) ) exit;

?>

<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/><![endif]-->

<title><?php echo get_the_title() ?></title>
<meta name="author" content="GR Test">
<meta name="description" content="A Simple Wp test for GR">

<meta name="viewport" content="width=device-width, initial-scale = 1, maximum-scale=1" />

<!-- Favs -->
<?php if (has_site_icon()) : wp_site_icon();  endif; ?>

<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">

<?php wp_head(); ?>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
</head>

<body>
