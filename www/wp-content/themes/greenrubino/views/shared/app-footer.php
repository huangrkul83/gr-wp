<?php
/**
 * Views/Shared/Footer
 *
 * Global footer element, inlcuding wp_footer().
 *
 * @author    You
 * @package   jumpoff
 * @version   1.0
 */

namespace Jumpoff;

if ( ! defined( 'ABSPATH' ) ) exit;

?>

<footer class="app-footer"></footer>

<?php wp_footer(); ?>

</body>
</html>
