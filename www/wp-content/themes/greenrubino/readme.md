# GR Dev Test

Welcome to the GR Dev test. Thanks for playing.


### Overview
This test involves simple Wordpress and front-end development as those skills are important at GR.
You are asked to use Wordpress to register a post type called 'Team', and build out a simple archive view.
The test should take less than 2 hours.


### Dependencies / Requirements.
- Use `sass/scss`. Add `sass/scss` to `src/assets/src`
- Compile `src/assets/scss/app.scss` to `assets/css/app.css`. That location is already enqueued/loaded by Wp.
- Use `Gulp` to compile `sass/scss` - how ever you want to configure that is fine.
- Don't Use Bootstrap or similar css framework.
- This starter theme uses [StoutLogic's ACF Builder](https://github.com/StoutLogic/acf-builder), which is a more sane way to register ACF fields within PHP utils. More on that below. Composer is required to install.
- This starter uses a simple namespace of `Jumpoff` to scope functions.

*NOTE* If you want to completely replace this theme with your own, and can accomplish the same ends, please feel free. This starter is just a stripped down example of a common Wp environment at GR. If you do replace, just make sure it's truly your code and not an existing theme.


### Run / Start

1. Clone this repo and add it to the 'themes' directory of your Wp setup.
2. Install [StoutLogic's ACF Builder](https://github.com/StoutLogic/acf-builder) via composer

```
composer install
```

3. Install and activate the provided version of ACF Pro (`inc/required-plugins/acf-pro`)

4. Setup you gulp tasks for scss/sass.

5. Activate Theme


## The Test


### 1. Create a Post Type Called Team
- Create a Team Post Type, using the provided helper at `inc/PostTypes/setLabels.php`.
- An example post type is provided at `inc/PostTypes/Example.php`.
- It should be hierarchal, with a single and archive view, with a slug of `team`.
- Add the post type registration to the provided file at `inc/PostTypes/Team.php`. It will be automatically loaded for you.
- *Note* that this theme starter uses a custom template loader, so add you code to the provided files at:

*Team Archive* `views/team/archive.php`

*Team Single* `views/team/single.php`


### 2. Add Custom Fields to the Team Post Type

- Install ACF Pro (provided)
- This theme uses [StoutLogic's ACF Builder](https://github.com/StoutLogic/acf-builder) to handle registration of ACF fields.
- See inc/Fields/Team for the Team specific fields.

*Existing Fields*:

-- `team_image` (image)
-- `team_position` (text)

- Add a `team_bio` (textarea or wysi) field following the existing fields (for a super basic single view).
- Use the default Wp Title for Team Name
- Super Helpful: [StoutLogic ACF Builder Cheatsheet](https://gist.github.com/stephenscaff/7eaf497fa01bea5b9b5a939539412b2c)


### 3. Build out design
- Refer to the simple design to build out a simple team page.
- The design is more to see how you build things out, apply your own design sense to fonts and images.
- Focus on the Team Archive view, use a simple `get_posts()` query to pull all Team posts.
- Extra points for a basic team single view, using the `team_bio` field you added.
- You pick fonts (Google fonts is fine) and imagery of team members.
- Hint - [Source.Unsplash.com](https://source.unsplash.com/)

*The Design*

![Design](/gr-test-team.jpg?raw=true "Design")


### 4. Submit Back to GR

- Take a screen shot of your final layout and send add it to the theme directory
- Create and push to a public repo and send use the link
- Crazy extra points if you provide a live link. Otherwise, we'll just add your code to wp build and create a few team posts to see you design.
