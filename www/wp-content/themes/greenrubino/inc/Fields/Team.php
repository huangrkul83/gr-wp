<?php

if ( ! defined( 'ABSPATH' ) ) exit;

use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 *   Mast Fields
 *   For mastheads and stuff.
 */

$team_fields = new StoutLogic\AcfBuilder\FieldsBuilder('team',
  [
    'position'   => 'acf_after_title',
  ]
);

$team_fields
  ->addImage('team_image', [
    'label' => 'Team Image <br/><span>Size to 1250x1500</span>'
  ])
  ->addText('team_position')
  ->addTextarea('team_bio')
  ->setLocation('post_type', '==', 'team');

  add_action('acf/init', function() use ($team_fields) {
   acf_add_local_field_group($team_fields->build());
});
