<?php

/**
 *  Admin Dash View
 *
 *
 *  @version    1.0
 *  @see        admin-dash.php
 *  @see        admin/admin-theme/assets (for styles)
 */

if ( ! defined( 'ABSPATH' ) ) exit;

# Wp admin bootstrap
require_once( ABSPATH . 'wp-load.php' );
require_once( ABSPATH . 'wp-admin/admin.php' );
require_once( ABSPATH . 'wp-admin/admin-header.php' );

?>

<section class="dash">

  <header class="dash-header">
    <h1 class="dash-header__title">Welcome to the GR Test Theme</h1>
    <p class="dash-header__text">It has just enough to get you going</p>
  </header>

  <section>
    <p>*NOTE* If you want to completely replace this theme with your own, and can accomplish the same ends, please feel free. This starter is just a stripped down example of a common Wp environment at GR. If you do replace, just make sure it's truly your code and not an existing theme.</p>
    <br/>
    <h3>1. Create a Post Type Called Team</h3>
    <p>- Create a Team Post Type, using the provided helper at <code>inc/PostTypes/setLabels.php</code>.</p>
    <p>- An example post type is provided at <code>inc/PostTypes/Example.php</code>.</p>
    <p>- It should be hierarchal, with a single and archive view</p>
    <p>- Add the post type registration to the provided file at <code>inc/PostTypes/Team.php</code>. It will be automatically loaded for you.</p>
    <p>- Register the slug as <code>'team'</code></p>
    <p>- Note that this theme starter uses a custom template loader, so add Team single/archive code to:</p>
    <p><strong>Team Single</strong> <code>views/team/single.php</code></p>
    <p><strong>Team Archive</strong> <code>views/team/archive.php</code></p>
    <br/>

    <h3>2. Add Custom Fields to the Team Post Type</h3>

    <p>- Install ACF Pro (provided)</p>

    <p>- This theme uses <a href="https://github.com/StoutLogic/acf-builder" target="_blank">Stout Logic's ACF Builder</a> to handle registration of ACF fields.</p>

    <p>- See <code>inc/Fields/Team</code> for the Team specific fields.</p>
    <p>Currently you have:</p>
    <p>-- team_image (image)</p>
    <p>-- team_position (text)</p>

    <p>- Add a team_bio (textarea or wysi) field following the existing fields as example.</p>
    <p>- Use the Default Title for Team Name</p>

    <p>- Helpful: <a href="https://gist.github.com/stephenscaff/7eaf497fa01bea5b9b5a939539412b2c" target="_blank">StoutLogic ACF Builder Cheatsheet</a></p>

    <br/>
    <h3>3. Build out design  </h3>
    <p> Refer to the simple design to build out a simple team page.</p>
    <p>The design is more to see how you build things out, apply your own design sense to fonts and images.</p>
    <p>DO NOT USE BOOTSTRAP. This is simple layout.</p>
    <p> Focus on the Team Archive view, use a simple <a href="https://codex.wordpress.org/Template_Tags/get_posts">get_posts()</a> query to pull all Team posts.</p>
    <p>- Use sass and gulp</p>
    <p>- Extra points for quality font selection and image placeholder service</p>
    <p>- Google fonts are fine</p>
    <p>- Hint - Unsplash has a nice placeholder service <a href="https://source.unsplash.com/">Source.Unsplash.com</a></p>

    <br/>
    <h3>4. Submit Back</h3>
    <p>Take a screen shot of your final layout and send add it to the theme directory</p>
    <p>Create and push to a public repo and send use the link</p>
    <p>Super, super extra points for a live link. Otherwise, we'll just load your theme and create a few Team Posts to check the design in action</p>
  </section>

</section>
