<?php

namespace Jumpoff;



if ( ! defined( 'ABSPATH' ) ) exit;

/**
 *  Featured Image Helper
 *
 * @example: jumpoff_ft_img('full')
 *
 * @param array/string $size  images size - ie; full, medium, small)
 * @param string $id optional image id
 * @return string Image Url
 */

function get_ft_img($size, $post_id = '', $fallback = false) {
  global $post, $posts;

  // Allow for specific Image Ids
  if ($post_id) {
    $post = get_post($post_id);
    setup_postdata( $post_id );
  }

  // Read featured image data for image url.
  $image_id = get_post_thumbnail_id();

  // Get Image src of image attached to post.
  $attached_to_post = wp_get_attachment_image_src( get_post_thumbnail_id(), $size, false);

  // Set our attached image as the returned related image.
  $img =  $attached_to_post[0];

  // Get Alert
  $alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);

  // get $caption
  $caption = wp_get_attachment_caption( $image_id );


  if ($post_id) {
    wp_reset_postdata( $post_id );
  }

  $img_obj = array(
    'url' => $img,
    'alt' => $alt,
    'caption' => $caption
  );
  return (object)$img_obj;
}
